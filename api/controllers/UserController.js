/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var passport = require('passport');
var crypto = require('crypto');

module.exports = {
    passport_local: [
        passport.authenticate('local'),
        function(req,res){
            res.send(req._passport.session.user);
        }
    ],
    passport_facebook: [
        passport.authenticate('facebook-token'),
        function(req,res){
            res.send(req._passport.session.user);
        }
    ],
    passport_twitter: [
        passport.authenticate('twitter-token'),
        function(req,res){
            res.send(req._passport.session.user);
        }
    ],
    passport_google_web: [
        passport.authenticate('google-web'),
        function(req,res){
            res.send(req._passport.session.user);
        }
    ],
    passport_google_ios: [
        passport.authenticate('google-ios'),
        function(req,res){
            res.send(req._passport.session.user);
        }
    ],
    passport_google_android: [
        passport.authenticate('google-android'),
        function(req,res){
            res.send(req._passport.session.user);
        }
    ],

    logout: function(req,res){
        sails.models.user.findOne({id:req.user.id})
        .then(function(user){
            if (!user) res.serverError('Invalid User');
            else{
                sails.models.token.destroy({value: req.user.token})
                .then(function(){
                    res.send();                    
                })
                .catch(function(){
                    res.send();
                })
            }
        })
        .catch(function(err){
            res.serverError(err);
        })
    },
    createUser: function(req,res){
        var body = req.body;
        if ((!body) || (!body.username) || (!body.password) || (!body.email)) res.forbidden('Username, email or password missing');
        else{

            var data = {
                username: body.username,
                password: body.password,
                email: body.email,
                role: 'user',
                vat: body.vat,
                name: body.name,
                age: body.age,
                gender: body.gender,
                birthday: body.birthday,
                address: body.address,
                location: body.location,
                link: body.link
            };

            crypto.randomBytes(48, function(ex, buf) {

                if (sails.config.register.activate){
                    var activationToken = buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-');
                    data.activationCode = activationToken;
                    data.activated = false;
                }

                sails.models.user.create(data).then(function(user){
                    
                    if (sails.config.register.activate) utilsMail.sendActivationMail(user.email,user.activationCode,function(err){
                        if (!!err) res.serverError("Can't send activation mail: "+err);
                        else res.send(user.sanitize());
                    });
                    else res.send(user.sanitize());
                })
                .catch(function(err){
                    if (err.code=='E_VALIDATION') res.badRequest(err,{forceOutput:true});
                    else if (!!err.stack) res.serverError(err.stack);
                    else res.serverError(err);
                });
            });
        }
    },
    activate: function(req,res){


        sails.models.user.update({activationCode:req.params.activationCode},{activated:true})
        .then(function(user){
            if (!user) res.forbidden('Invalid Activation Code');
            else res.send();
        })
        .catch(function(err){
            res.serverError(err);
        })

    },
    update: function(req,res){
        var id = req.params.id;
        var body = req.body;
        var reset = false;
        if ((!!body) && (body.reset)) {
            reset = true;
            delete body.reset;
            body.password = utilsCrypt.simplePassword(5);
        }
        var password = body.password;
        if (!id) res.badRequest();
        else sails.models.user.update({id:id},body).then(function(user){
            if (!user.length) res.badRequest();
            else if (reset){
                //reset password
                user[0].password = password;
                utilsMail.sendPasswordMail(user[0].email,user[0].username,user[0].password);
                res.send(user[0]);
            }
            else res.send(user[0]);
        }) 
        .catch(function(err){
            res.serverError(err);
        })
    },
    findOne: function(req,res){
        if (req.user.id!=req.params.id) res.forbidden();
        else{
            res.send(req.user);
        }
    }
};