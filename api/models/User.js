/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    email: "string",
    username: {
      type: "string",
      unique: true
    },
    social_username: "string",
    name: "string",
    password: "string",
    role: "string",
    activationCode: "string",
    activated: "boolean",
    tokens:{
      collection: 'token',
      via: 'user'
    },
    socialID: 'string',
    social: 'string',
    age: "integer",
    gender: "string",
    birthday: "string",
    address: "string",
    vat: "string",
    image: "string",
    location: "string",
    link: "string",
    sanitize: function(){
      delete this.password;
      delete this.token;
      delete this.activationCode;
      return this;
    }
  },
  beforeCreate: function (attrs, next) {

    if (!!attrs.password){
      var bcrypt = require('bcrypt');

      bcrypt.genSalt(10, function(err, salt) {
        if (err) next(err);

        bcrypt.hash(attrs.password, salt, function(err, hash) {
          if (err) next(err);

          attrs.password = hash;
          next();
        });
      });
    }
    else next();
  },
  beforeUpdate: function(attrs,next){

    if (!!attrs.password){

        var bcrypt = require('bcrypt');

        bcrypt.genSalt(10, function(err, salt) {

          if (err) next(err);

          bcrypt.hash(attrs.password, salt, function(err, hash) {

            if (err) next(err);

            attrs.password = hash;
            next();
          });
        });

    }
    else next();
  }
};