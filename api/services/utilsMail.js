// var nodemailer = require('nodemailer');

var nodemailer = require('nodemailer');

var utilsMail = {
    sendActivationMail: function(email,token,cb){

        if ((!!email) && (!!token)){
            var transporter = nodemailer.createTransport("SMTP",{
                service: sails.config.deyob.email.service,
                auth: {
                    user: sails.config.deyob.email.user,
                    pass: sails.config.deyob.email.pass
                }
            });
            

            var activationLink = sails.config.deyob.registerUrl+"/"+token;
            var message = "<p>Thank you for registering.<p>Please click the link below to activate your account.</p><a href='"+activationLink+"'>"+activationLink+"</a>";
            transporter.sendMail({
                from: 'deYob'+" <filippo@crispybacon.it>", // sender address
                to: email, // list of receivers
                subject: 'deYob Activation Code', // Subject line
                html: message
            },function(err,info){
                if (!!cb){
                    if (!!err) cb(err); 
                    else cb();
                }
            });
        }
        else if (!!cb) cb('Invalid email or token');

    }
};

module.exports = utilsMail;