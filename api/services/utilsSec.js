var utilsSec = {
    genToken: function(id,cb){
        if (!!id){
            sails.models.user.findOne({id:id})
            .then(function(user){
                if ((!user) && (!!cb)) cb('Invalid ID');
                else if (!!user) return [
                    sails.models.token.create({user:id,value:utilsCrypt.uid(16)}),
                    user];
            })  
            .spread(function(token,user){
                user.sanitize();
                user.token = token.value;
                if (!!cb) cb(null,user);
            })
            .catch(function(err){
                if (!!cb) cb(err);
            })
        }
        else if (!!cb) cb('Invalid ID');
    }
};

module.exports = utilsSec;