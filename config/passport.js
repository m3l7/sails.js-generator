var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy
    , BearerStrategy = require('passport-http-bearer').Strategy
    , bcrypt = require('bcrypt')
    , FacebookTokenStrategy = require('passport-facebook-token').Strategy
    , TwitterTokenStrategy = require('passport-twitter-token').Strategy
    , GooglePlusStrategy = require('passport-google-plus')
    , config = require('../loadSailsrc')


passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(id, done) {
    sails.models.user.findOneById(id).done(function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    },
    function(username, password, done) {
        if ((!username) || (!password)) return done(null, false, { message: 'Utente o Password sbagliati.'});

        sails.models.user.findOne({ username: username})
        .then(function(user) {
            if (!user) { return done(null, false, { message: 'Utente o Password sbagliati.'}); }
            else if (!user.activated) return done(null, false, { message: 'Utente o Password sbagliati.'});
            else{
                bcrypt.compare(password, user.password, function(err, resp) {
                    if (!!err) return done(err);
                    else if (!resp) return done(null,false,{message: 'Utente o Password sbagliati.'})
                    else utilsSec.genToken(user.id,done);
                })
            }
        })
        .catch(function(err){
            return done(err);
        });
    }
));

passport.use(new BearerStrategy(
  function(accessToken, done) {
    sails.models.token.findOne({'value':accessToken})
    .populate('user')
    .then(function(tokenObj) {
        if (!tokenObj) return done(null,false);
        else if(!tokenObj.user) return done(null,false);
        else{
            var token = tokenObj.value;
            var user = tokenObj.user.sanitize();
            user.token = token;
            return done(null,user);
        }
    })
    .catch(function(err){
        return done(err);
    })
  }
));
passport.use(new FacebookTokenStrategy({
    clientID: config.config.deyob.facebook.clientID,
    clientSecret: config.config.deyob.facebook.clientSecret
  },
  function(accessToken, refreshToken, profile, done) {
    sails.models.user.findOne({socialId:profile.id})
    .then(function(user){
        if (!user){
            //create new user

            var age = null;
            if (!!profile._json.birthday){
                var d = new Date();
                var birth = profile._json.birthday; 
                d.setMonth(parseInt(birth.split('/')[0])-1);
                d.setDate(parseInt(birth.split('/')[1]));
                d.setYear(parseInt(birth.split('/')[2]));
                var t = new Date()-d;
                var age = Math.floor(t/1000/3600/24/365);
            }

            var user = {
                email: profile.emails.length ? profile.emails[0].value : '',
                username: 'fb_'+profile.id,
                social_username: profile.displayName,
                name: profile.displayName,
                password: '',
                role: 'user',
                activated: true,
                socialID: profile.id,
                social: 'facebook',
                age: age,
                birthday: birth,
                gender: profile._json.gender,
                link: profile._json.link,
                image: ((!!profile.photos) && (profile.photos.length)) ? profile.photos[0].value : null
            };

            return sails.models.user.create(user);
        }
        else return user;
    })
    .then(function(user){
        if (!user) return done("Can't login or register",null);
        else utilsSec.genToken(user.id,done);
    })
    .catch(function(err){
        return done(err,null);
    })
  }
));

passport.use(new TwitterTokenStrategy({
    consumerKey: config.config.deyob.twitter.consumerKey,
    consumerSecret: config.config.deyob.twitter.consumerSecret,
  },
  function(token, tokenSecret, profile, done) {
    sails.models.user.findOne({socialId:profile._json.id})
    .then(function(user){
        if (!user){
            //create new user
            var user = {
                username: 'tw_'+profile._json.id,
                social_username: profile.username,
                name: profile._json.name,
                password: '',
                role: 'user',
                activated: true,
                socialID: profile._json.id,
                social: "twitter",
                image: profile._json.profile_image_url,
                location: profile._json.location,
                link: 'https://twitter.com/'+profile.username
            };

            return sails.models.user.create(user);
        }
        else return user;
    })
    .then(function(user){
        if (!user) return done("Can't login or register",null);
        else utilsSec.genToken(user.id,done);
    })
    .catch(function(err){
        return done(err,null);
    })

  }
));

passport.use('google-web',new GooglePlusStrategy({
    clientId: config.config.deyob.google.webClientID,
    apiKey: config.config.deyob.google.apiKey
  },
  function(tokens, profile, done) {
    // Create or update user, call done() when complete... 
    loginGoogleProfile(profile,done);
  }
));

passport.use('google-android',new GooglePlusStrategy({
    clientId: config.config.deyob.google.androidClientID,
    apiKey: config.config.deyob.google.apiKey
  },
  function(tokens, profile, done) {
    // Create or update user, call done() when complete... 
    loginGoogleProfile(profile,done);
  }
));

passport.use('google-ios',new GooglePlusStrategy({
    clientId: config.config.deyob.google.iosClientID,
    apiKey: config.config.deyob.google.apiKey
  },
  function(tokens, profile, done) {
    // Create or update user, call done() when complete... 
    loginGoogleProfile(profile,done);
  }
));

function loginGoogleProfile(profile,done){

    sails.models.user.findOne({socialId:profile.id})
    .then(function(user){
        if (!user){
            //create new user

            var location = '';
            if (profile.placesLived.length){
                profile.placesLived.forEach(function(place){
                    if (place.primary) location = place.value;
                })
                location = location || placesLived[0].value;
            }

            var user = {
                username: 'go_'+profile.id,
                social_username: profile.displayName,
                name: profile.displayName,
                password: '',
                role: 'user',
                activated: true,
                socialID: profile.id,
                social: "google",
                image: (!!profile.image) ? profile.image.url : '',
                location: location,
                link: profile.url,
                birthday: profile.birthday,
                gender: profile.gender
            };

            return sails.models.user.create(user);
        }
        else return user;
    })
    .then(function(user){
        if (!user) return done("Can't login or register",null);
        else utilsSec.genToken(user.id,done);
    })
    .catch(function(err){
        return done(err,null);
    })

}