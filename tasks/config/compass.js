/**
 * Compiles SASS files into CSS.
 *
 * ---------------------------------------------------------------
 */
module.exports = function(grunt) {

  grunt.config.set('compass', {
    options: {
      sassDir: 'assets/content/styles',
      cssDir: '.tmp/public/css/',
      generatedImagesDir: '.tmp/public/images/generated',
      imagesDir: 'assets/content/images',
      javascriptsDir: 'assets/scripts',
      fontsDir: 'assets/styles/fonts',
      importPath: './bower_components',
      httpImagesPath: '/content/images',
      httpGeneratedImagesPath: '/content/images/generated',
      httpFontsPath: '/styles/fonts',
      relativeAssets: false,
      assetCacheBuster: false,
      raw: 'Sass::Script::Number.precision = 10\n'
    },
    dev: {
      options: {
        debugInfo: true
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-compass');
};