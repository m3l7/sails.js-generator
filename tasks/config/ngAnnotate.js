// Performs rewrites based on filerev and the useminPrepare configuration
module.exports = function(grunt) {

  grunt.config.set('ngAnnotate', {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: ['*.js', '!oldieshim.js'],
          dest: '.tmp/concat/scripts'
        }]
      }
  })

  grunt.loadNpmTasks('grunt-ng-annotate');
};