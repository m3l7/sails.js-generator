// Performs rewrites based on filerev and the useminPrepare configuration
module.exports = function(grunt) {

  grunt.config.set('usemin', {
		  html: ['.tmp/public/{,*/}*.html'],
		  css: ['.tmp/public/css/{,*/}*.css'],
		  options: {
		    assetsDirs: ['./tmp/public/','./tmp/public/content/images','./tmp/public/content/fonts']
		  }
	})

	grunt.loadNpmTasks('grunt-usemin');
};