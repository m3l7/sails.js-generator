module.exports = function(grunt) {

  grunt.config.set('useminPrepare', {
    html: '.tmp/public/index.html',
    options: {
      dest: '.tmp/public/',
      flow: {
        html: {
          steps: {
            js: ['concat', 'uglifyjs'],
            // js: ['concat'],
            css: ['cssmin']
          },
          post: {}
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-usemin');
};