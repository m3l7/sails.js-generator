module.exports = function (grunt) {
    grunt.registerTask('buildProd', [
        'compileAssets',
        'injectEmptyMock',
        'useminPrepare',
        'concat:generated',
        'ngAnnotate',
        'uglify:generated',
        'cssmin:generated',
        'filerev',
        'usemin',
        'clean:build',
        'copy:build'
    ]);
};
