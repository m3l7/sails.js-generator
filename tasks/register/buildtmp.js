module.exports = function (grunt) {
    if (process.env.MOCK=='true') grunt.registerTask('buildtmp', [
        'compileAssets',
        'injectMock',
        'useminPrepare',
        'concat:generated',
        // 'ngAnnotate',
        'uglify:generated',
        'cssmin:generated',
        // 'filerev',
        'usemin'
    ]);
    else grunt.registerTask('buildtmp', [
        'compileAssets',
        'injectEmptyMock',
        'useminPrepare',
        'concat:generated',
        // 'ngAnnotate',
        'uglify:generated',
        'cssmin:generated',
        // 'filerev',
        'usemin'
    ]);
};
