module.exports = function (grunt) {

    if (process.env.MOCK=='true') grunt.registerTask('default', [
        'compileAssets',
        'injectMock',
        'sails-linker:liveReload',
        'watch:assetsMock'
    ]);
    else grunt.registerTask('default', [
        'compileAssets',
        'injectEmptyMock',
        'sails-linker:liveReload',
        'watch:assets'
    ]);
};
